package com.pm.playmanager;

public enum Posicion {
	PG("PG"),
	SG("SG"),
	SF("SF"),
	PF("PF"),
	C("C");
	
	private final String denominacion;
	
	Posicion(String den){
		this.denominacion = den;
	}
	
	@Override
	public String toString(){
		return this.denominacion;
	}
}
