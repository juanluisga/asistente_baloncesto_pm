package com.pm.playmanager;

import com.pm.util.*;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Plantilla {
	
	private ArrayList<Jugador> plantilla;
	private String status = "Plantilla sin cargar";
	 
	public Plantilla(){
		;
	}
	
	public void cargarFichero(String fichero, String codificacion, String separador, boolean conCabecera)
	   throws UnsupportedEncodingException, FileNotFoundException, IOException{
	       
		FicheroCSV ficheroCSV = new FicheroCSV(fichero,codificacion,separador,conCabecera);
		ArrayList<String[]> jugadores = ficheroCSV.Leer();
		Iterator<String[]> i = jugadores.iterator();
		plantilla = new ArrayList<Jugador>();
		while (i.hasNext()) plantilla.add(new Jugador(i.next()));
		status = "Plantilla cargada desde el fichero ".concat(fichero);	    
	}
	
	public String getStatus(){
	   return status;    
	}

    public String[] getInfo(){
        return new String[]{"NOM", "POS", "PR", "LNZ", "BLK", 
                    "PAS", "TEC", "VEL", "AGR", "SAL", "EXP", "ALT","VG"};        
    }

/*
	public String[][] toStringArray(){
	   String[][] plantilla = new String[this.plantilla.size()][this.getInfo().length];
	   int i = 0;
	   for (Jugador jugador : this.plantilla){
	       plantilla[i] = jugador.toStringArray();
	       i++; 
	   }
	   return plantilla;
	}
*/

    public ArrayList<Hashtable<String, String>> toHash(){
        ArrayList<Hashtable<String, String>> lista_plantilla = new ArrayList<Hashtable<String, String>>();
        for (Jugador jugador : this.plantilla){
            lista_plantilla.add(jugador.toHash());
        }    
        return lista_plantilla;
    }
	
	public ArrayList<Jugador> candidatos(Posicion posicion){
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
				
		Iterator<Jugador> i = plantilla.iterator();
		while (i.hasNext()) {
			Jugador jugador = i.next();
			if (jugador.valoracion(posicion)>0 ) jugadores.add(jugador);
		}
		return jugadores;
	}
	
	public void asignarPosiciones(){
	
		int jug_puesto = plantilla.size()/5;
	}
	
}
