package com.pm.playmanager;

import java.util.Hashtable;

/**
 * Write a description of class Jugador here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Jugador{
	// constantes
	static final float MUCHA_INFLUENCIA 	= 1;
	static final float GRAN_INFLUENCIA 		= 0.8f;
	static final float MEDIA_INFLUENCIA		= 0.5f;
	static final float POCA_INFLUENCIA		= 0.3f;
	static final float MINIMA_INFLUENCIA	= 0.1f;
	
	
    // instance variables - replace the example below with your own
	private String		nombre;
    private Posicion    posicion;
    private String      proyeccion;
    private int       	lanzamiento;
    private int       	bloqueo;
    private int       	pase;
    private int       	tecnica;
    private int       	velocidad;
    private int       	agresividad;
    private int       	salto;
    private int       	experiencia;
    private int       	altura;
    private int			valorPG;
    private int			valorSG;
    private int			valorSF;
    private int			valorPF;
    private int 		valorC;
    
    /**
     * Constructor for objects of class Jugador
     */
    public Jugador(String nombre, String posicion, String proyeccion, short lanzamiento, short bloqueo, short pase, short tecnica, short velocidad, short agresividad, short salto, short experiencia, short altura)
    {
    	setNombre(nombre);
    	//setPosicion(Posicion.valueOf(posicion));
    	setProyeccion(proyeccion);
    	setLanzamiento(lanzamiento);
    	setBloqueo(bloqueo);
    	setPase(pase);
    	setTecnica(tecnica);
    	setVelocidad(velocidad);
    	setAgresividad(agresividad);
    	setSalto(salto);
    	setExperiencia(experiencia);
      setAltura(altura);
      valorar();
    }
    
    public Jugador(String[] registro){
    	setNombre(registro[0]);
		//setPosicion(Posicion.valueOf(registro[1]));
		setProyeccion(registro[5]);
		setLanzamiento(Integer.parseInt(registro[6]));
		setBloqueo(Integer.parseInt(registro[7]));
		setPase(Integer.parseInt(registro[8]));
		setTecnica(Integer.parseInt(registro[9]));
		setVelocidad(Integer.parseInt(registro[10]));
		setAgresividad(Integer.parseInt(registro[11]));
		setSalto(Integer.parseInt(registro[12]));
		setExperiencia(Integer.parseInt(registro[13]));
		setAltura(Integer.parseInt(registro[15]));
		valorar();
    }
    
    public Jugador(){
    	;
    }

    /* METODOS PRIVADOS */
    private void valorar(){
    	    	
    	// PG:
    	if (altura < 190) {
    		valorPG = Math.round(
    	    							pase * MUCHA_INFLUENCIA + 
    	    							(tecnica + velocidad) * GRAN_INFLUENCIA + 
    	    							(agresividad + salto) * MINIMA_INFLUENCIA);
    	} else valorPG = 0;    			
    	// SG:
    	if (altura > 184 && altura < 201){
    	    this.valorSG = Math.round(
    	    							(pase + tecnica) * GRAN_INFLUENCIA + 
    	    							velocidad * MEDIA_INFLUENCIA + 
    	    							(agresividad + salto) * POCA_INFLUENCIA);
    	} else valorSG = 0;
    	// SF:
    	if (this.altura > 189 && altura < 206){
    		valorSF = Math.round((pase + tecnica + velocidad + agresividad + salto) * MEDIA_INFLUENCIA);
    	} else valorSF = 0;
    	// PF:
    	if (altura > 199 && altura < 216){
    		valorPF = Math.round(
    	    					(agresividad + salto) * GRAN_INFLUENCIA + 
    	    					velocidad * MEDIA_INFLUENCIA + (pase+tecnica) * POCA_INFLUENCIA); 
    	} else valorPF = 0;
    	// C:	
    	if (altura > 204){
    		valorC = Math.round(
    	    					(agresividad + salto) * MUCHA_INFLUENCIA + 
    	    					(tecnica + velocidad) * POCA_INFLUENCIA + 
    	    					pase * MINIMA_INFLUENCIA);
    	} else valorC = 0;
    	   	
    }
    
    
    
    /* METODOS PUBLICOS */
    public void setNombre(String nombre){
        this.nombre = nombre;
    }   
    
    public String getNombre(){
    	return this.nombre;
    }
    
    public void setPosicion(Posicion posicion){
        this.posicion = posicion;
    }
    
    public Posicion getPosicion(){
        return this.posicion;    
    }
    
    public void setProyeccion(String proyeccion){
        this.proyeccion = proyeccion;
    }
    
    public String getProyeccion(){
        return this.proyeccion;    
    }    
    
    public void setLanzamiento(int lanzamiento){
        this.lanzamiento = lanzamiento;
    }
    
    public int getLanzamiento(){
        return this.lanzamiento;    
    }
    
    public void setBloqueo(int bloqueo){
        this.bloqueo = bloqueo;
    }
    
    public int getBloqueo(){
        return this.bloqueo;    
    }
    
    public void setPase (int pase){
        this.pase = pase;
    }
    
    public int getPase(){
        return this.pase;    
    }
    
    public void setTecnica(int tecnica){
        this.tecnica = tecnica;
    }
    
    public int getTecnica(){
        return this.tecnica;    
    }
    
    public void setVelocidad(int velocidad){
        this.velocidad = velocidad;
    }
    
    public int getVelocidad(){
        return this.velocidad;    
    }
    
    public void setAgresividad(int agresividad){
        this.agresividad = agresividad;
    }
    
    public int getAgresividad(){
        return this.agresividad;    
    }
    
    public void setSalto(int salto){
        this.salto = salto;
    }
    
    public int getSalto(){
        return this.salto;
    }
    
    public void setExperiencia (int experiencia){
        this.experiencia = experiencia;
    }
    
    public int getExperiencia(){
        return this.experiencia;    
    }
    
    public void setAltura (int altura){
        this.altura = altura ;
    }
    
    public int getAltura(){
        return this.altura;    
    }
    
    public int getValoracionGeneral(){
        return 	this.lanzamiento + 
        		this.bloqueo + 
        		this.pase + 
        		this.tecnica + 
        		this.velocidad + 
        		this.agresividad + 
        		this.salto;
    }
    
    public int valoracion(Posicion posicion){
    	int valor = 0;
    	switch(posicion){
    		case PG:
    			valor = this.valorPG;
    			break;
    		case SG:
    			valor = this.valorSG;
    			break;
    		case SF:
    			valor = this.valorSF;
    			break;
    		case PF:
    			valor = this.valorPF;
    			break;
    		case C:
    			valor = this.valorC;
    			break;
    	}
    	return valor;
    }

/*
    public String[] toStringArray(){
        String[] jugador = new String[]{    this.getNombre(),
                                //this.getPosicion().toString(),
                                "---",
                                this.getProyeccion(),
                                Integer.toString(this.getLanzamiento()),
                                Integer.toString(this.getBloqueo()),
                                Integer.toString(this.getPase()),
                                Integer.toString(this.getVelocidad()),
                                Integer.toString(this.getTecnica()),
                                Integer.toString(this.getAgresividad()),
                                Integer.toString(this.getSalto()),
                                Integer.toString(this.getExperiencia()),
                                Integer.toString(this.getAltura()),
                                Integer.toString(this.getValoracionGeneral())
                        }; 
        return jugador;
    }
*/    

    public Hashtable<String, String> toHash(){
        Hashtable<String, String> jugador = new Hashtable<String, String>();
        
        jugador.put("NOM", this.getNombre());
        jugador.put("POS", "---");
        jugador.put("PR", this.getProyeccion());
        jugador.put("LNZ", Integer.toString(this.getLanzamiento()));
        jugador.put("BLK", Integer.toString(this.getBloqueo()));
        jugador.put("PAS", Integer.toString(this.getPase()));
        jugador.put("TEC", Integer.toString(this.getTecnica()));
        jugador.put("VEL", Integer.toString(this.getVelocidad()));
        jugador.put("AGR", Integer.toString(this.getAgresividad()));
        jugador.put("SAL", Integer.toString(this.getSalto()));
        jugador.put("EXP", Integer.toString(this.getExperiencia()));
        jugador.put("ALT", Integer.toString(this.getAltura()));
        jugador.put("VG", Integer.toString(this.getValoracionGeneral()));
        
        return jugador;
    }
    
    public String[] registro(){
    	String[] reg = new String[6];
    	int valor;
    	reg[0] = getNombre();
		valor = valoracion(Posicion.PG);
		reg[1]=(valor > 0)?Integer.toString(valor):"--";
		valor = valoracion(Posicion.SG);
		reg[2]=(valor > 0)?Integer.toString(valor):"--";
		valor = valoracion(Posicion.SF);
		reg[3]=(valor > 0)?Integer.toString(valor):"--";
		valor = valoracion(Posicion.PF);
		reg[4]=(valor > 0)?Integer.toString(valor):"--";
		valor = valoracion(Posicion.C);
		reg[5]=(valor > 0)?Integer.toString(valor):"--";    	
    	    	  	
    	return reg;
    }
}
