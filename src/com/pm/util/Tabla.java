package com.pm.util;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;

public class Tabla{

    private int anchoCaracteres;
    private String titulo;
    private String[] cabecera;
    private boolean conBordes;
    private Hashtable<String, Integer> camposLongitud = new Hashtable<String, Integer>();    
    private String separador;
    
    
    private String[] tabla;
    
    public Tabla(){
        this.anchoCaracteres = 94;
        this.conBordes = true;
        this.separador = "║";
        this.camposLongitud.put("NOM", 25);
        this.camposLongitud.put("POS", 3);
        this.camposLongitud.put("PR", 3);
        this.camposLongitud.put("LNZ", 3);
        this.camposLongitud.put("BLK", 3);
        this.camposLongitud.put("PAS", 3);
        this.camposLongitud.put("TEC", 3);
        this.camposLongitud.put("VEL", 3);
        this.camposLongitud.put("AGR", 3);
        this.camposLongitud.put("SAL", 3);
        this.camposLongitud.put("EXP", 3);
        this.camposLongitud.put("ALT", 3);
        this.camposLongitud.put("VG", 3);
    }

    public void setAnchoCaracteres(int caracteres){
        // restamos 1 porque empezamos desde índice 0
        this.anchoCaracteres = caracteres - 1;    
    }
    
    public void setTitulo(String titulo){
        this.titulo = (titulo.length() % 2 == 0)?titulo:titulo.concat(" ");;    
    }
    
    public void setCabecera(String[] cabecera){
        this.cabecera = cabecera;    
    }
    
    public void setConBordes(boolean bordes){
        this.conBordes = bordes;    
    }
    
    public void setSeparador(String separador){
        this.separador = separador;
    }
    
    public String[] mostrarTabla(ArrayList<Hashtable<String, String>> datos, String[] cabecera){
        
        this.tabla = new String[datos.size() + 8];
        this.mostrarTitulo();
        this.mostrarCabecera(cabecera);
        this.mostrarContenido(datos);
        this.mostrarPie();

        return this.tabla;             
    }
    
    private void mostrarTitulo(){
        
        this.tabla[0] = "╔";
        for (int i = 1; i < this.anchoCaracteres; i++) this.tabla[0] = this.tabla[0].concat("═");
        this.tabla[0] = this.tabla[0].concat("╗");
        
        this.tabla[1] = "║";
        this.tabla[1] = this.tabla[1].concat(Util.centrarCadena(this.titulo, this.anchoCaracteres - 1));
        this.tabla[1] = this.tabla[1].concat("║");
        
        this.tabla[2] = "╠";
        for (int i = 1; i < this.anchoCaracteres; i++) this.tabla[2] = this.tabla[2].concat("═");
        this.tabla[2] = this.tabla[2].concat("╣");
    }
    
    private void mostrarCabecera(String[] cabecera){

        System.out.println("Ancho caracteres: " + this.anchoCaracteres);
        String texto;
        Enumeration<String> encabezado = this.camposLongitud.keys();
        this.tabla[3] = "║";
        
        for (String elemento : cabecera){
            texto = Util.centrarCadena(elemento, this.camposLongitud.get(elemento));
            this.tabla[3] = this.tabla[3].concat(texto);
            this.tabla[3] = this.tabla[3].concat(this.separador);                
        }        
        
        this.tabla[4] = "╠";
        for (int i = 1; i < this.anchoCaracteres; i++) this.tabla[4] = this.tabla[4].concat("═");
        this.tabla[4] = this.tabla[4].concat("╣");
    }
    
    private void mostrarContenido(ArrayList<Hashtable<String, String>> datos){
        
        int i = 5;
        Enumeration e;
        //String atributo;
        String valor;
        for (Hashtable<String, String> linea : datos){
            this.tabla[i] = "║";
            for (String atributo : this.cabecera) {
                valor = linea.get(atributo).toString();
                switch ( atributo ){
                    case    "NOM":
                    case    "POS":
                    case    "PR" :  valor = Util.justificarIzquierda(valor, this.camposLongitud.get(atributo));    
                                    break;
                    case    "LNZ":
                    case    "BLK":
                    case    "PAS":
                    case    "TEC":
                    case    "VEL":
                    case    "AGR":
                    case    "SAL":
                    case    "EXP":
                    case    "ALT":
                    case    "VG" :  valor = Util.justificarDerecha(valor, this.camposLongitud.get(atributo));
                                    break;

                    
                }
                this.tabla[i] = this.tabla[i].concat(valor);
                this.tabla[i] = this.tabla[i].concat(this.separador);
            }
            i++;
        }      
    }
    
    public void mostrarPie(){
        int tamTabla = this.tabla.length;
        this.tabla[tamTabla-3] = "╠";
        for (int i = 1; i < this.anchoCaracteres; i++) 
            this.tabla[tamTabla-3] = this.tabla[tamTabla-3].concat("═");
        this.tabla[tamTabla-3] = this.tabla[tamTabla-3].concat("╣"); 
        this.tabla[tamTabla-2] = this.separador;
        for (int i = 1; i < this.anchoCaracteres; i++) 
            this.tabla[tamTabla-2] = this.tabla[tamTabla-2].concat(" "); 
        this.tabla[tamTabla-2] = this.tabla[tamTabla-2].concat(this.separador);
        this.tabla[tamTabla-1] = "╚";
        for (int i = 1; i < this.anchoCaracteres; i++) 
            this.tabla[tamTabla-1] = this.tabla[tamTabla-1].concat("═");
        this.tabla[tamTabla-1] = this.tabla[tamTabla-1].concat("╝");                        
            
    }

}