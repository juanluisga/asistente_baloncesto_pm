package com.pm.util;

class Util{
    
    public static String concatenar(String cadena, String subcadena){
        cadena = cadena.concat(subcadena);
        return cadena;
    }
    
    public static String centrarCadena(String cadena, int espacio){
        String cadenaCentrada = "";
        int puntero = (espacio + 1 - cadena.length()) / 2;
        for (int i = 1; puntero > 0 && i < puntero; i++) 
            cadenaCentrada = cadenaCentrada.concat(" ");
        cadenaCentrada = cadenaCentrada.concat(cadena);
        for (int i = puntero - 1 + cadena.length(); puntero > 0 && i < espacio; i++) 
            cadenaCentrada = cadenaCentrada.concat(" ");
        return cadenaCentrada;        
    }
    
    public static String espaciosDerecha(String s, int n) {
        return String.format("%1$-" + n + "s", s);  
    }

    public static String espaciosIzquierda(String s, int n) {
        return String.format("%1$" + n + "s", s);  
    } 
    
    public static String justificarIzquierda(String cadena, int espacio){
        //return String.format( "%1$-" + Integer.toString( espacio - cadena.length() ) + "s", cadena );
        while (cadena.length() < espacio) cadena = cadena.concat(" ");
        return cadena; 
    }   
    
    public static String justificarDerecha(String cadena, int espacio){
        //return String.format( "%1$" + Integer.toString( espacio - cadena.length() ) + "s", cadena );
        while (cadena.length() < espacio) cadena = " ".concat(cadena);
        return cadena;
    }
}