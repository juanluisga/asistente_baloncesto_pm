
// paquete propio
import com.pm.playmanager.*;
import com.pm.util.*;

// paquetes standar
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class Asistente {

	public static void main(String[] args)throws UnsupportedEncodingException, IOException{
		
		Pantalla pantalla = new Pantalla();
		Plantilla plantilla = new Plantilla();
				
		pantalla.cabecera();
				
		while (true){
		    switch (pantalla.pedirOpcion()){
                case "0":
                    // salir
                    System.exit(0);
                    break;
                case "1":
                    // ayuda
                    pantalla.ayuda();
                    break;
                case "2":
                	// devolver ruta actual
                	String current = new java.io.File( "." ).getCanonicalPath();
                    System.out.println("Current dir:"+current);                	
                    // Cargar plantilla desde fichero
                    plantilla.cargarFichero(current + "/data/jugadores.csv","UTF8",";",true);
                    
                    break;
                case "3":
                    // Mostrar plantilla
                    pantalla.mostrarTabla(plantilla.toHash(), plantilla.getInfo());
                    break;
                case "4":
                	// Alineación 
                    break;
                case "5":
                    break;
                case "6":
                    break;
                case "7":
                    break;
                case "8":
                    break;
                case "9":
                    // status
                    pantalla.mostrarTexto(plantilla.getStatus());
                    break;
                   
        }
		      
		} 
		
        /*
		informeCSV.Escribir(informe);
		
		*/
		// posiciones
		
		/*
		Plantilla convocatoria = new Plantilla(plantilla);
		System.out.println("CANDIDATOS A BASE");
		System.out.println("=================");
		System.out.println();
		ArrayList<Jugador> bases = convocatoria.candidatos(Posicion.PG);
		Iterator<Jugador> j = bases.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A ESCOLTA");
		System.out.println("====================");
		System.out.println();
		ArrayList<Jugador> escoltas = convocatoria.candidatos(Posicion.SG);
		j = escoltas.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A ALERO");
		System.out.println("==================");
		System.out.println();
		ArrayList<Jugador> aleros = convocatoria.candidatos(Posicion.SF);
		j = aleros.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A ALA-PIVOT");
		System.out.println("======================");
		System.out.println();
		ArrayList<Jugador> alas = convocatoria.candidatos(Posicion.PF);
		j = alas.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A PIVOT");
		System.out.println("=================");
		System.out.println();
		ArrayList<Jugador> pivots = convocatoria.candidatos(Posicion.C);
		j = pivots.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
	*/
	}

}
