package playmanager_asistente;

import java.util.ArrayList;
import java.util.Iterator;

public class Plantilla {
	
	private ArrayList<Jugador> plantilla;
	
	public Plantilla(){
		;
	}
	
	public Plantilla(ArrayList<Jugador> plantilla){
		this.plantilla = plantilla;
	}
	
	public ArrayList<Jugador> candidatos(Posicion posicion){
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
				
		Iterator<Jugador> i = plantilla.iterator();
		while( i.hasNext() ) {
			Jugador jugador = i.next();
			if (jugador.valoracion(posicion)>0 ) jugadores.add(jugador);
		}
		return jugadores;
	}
	
	public void asignarPosiciones(){
	
		int jug_puesto = plantilla.size()/5;
	}
	
}
