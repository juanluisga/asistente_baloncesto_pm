package playmanager_asistente;

public enum Posicion {
	PG("Base"),
	SG("Escolta"),
	SF("Alero"),
	PF("Ala-Pivot"),
	C("Pivot");
	
	private final String denominacion;
	
	Posicion(String den){
		denominacion = den;
	}
	
	public String getDenominacion(){
		return denominacion;
	}
}
