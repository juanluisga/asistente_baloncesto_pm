package playmanager_asistente;

//import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

public class Asistente {

	public static void main(String[] args) 
			throws UnsupportedEncodingException, FileNotFoundException, IOException{
		
				
		// TODO Auto-generated method stub
		System.out.println("=========================================");
		System.out.println("= ASISTENTE DE PLAYMANAGER - BALONCESTO =");
		System.out.println("=========================================");
		System.out.println();
		
		FicheroCSV ficheroCSV = new FicheroCSV("./datos/jugadores.csv","UTF8",";",true);
		ArrayList<String[]> jugadores = ficheroCSV.Leer();
		Iterator<String[]> i = jugadores.iterator();
		ArrayList<Jugador> plantilla = new ArrayList<Jugador>();
		while( i.hasNext() ) plantilla.add(new Jugador(i.next()));
		
		FicheroCSV informeCSV = new FicheroCSV("./datos/informe.csv","UTF8",";",true);
		String[] cabecera = {"JUGADOR","BASE","ESCOLTA","ALERO","ALA","PIVOT"};
		informeCSV.setCabecera(cabecera);
		
		ArrayList<String[]> informe = new ArrayList<String[]>();
		Iterator<Jugador> jugador = plantilla.iterator();				
		while ( jugador.hasNext() )	informe.add(jugador.next().registro());
		informeCSV.Escribir(informe);
		
		/* posiciones */
		
		Plantilla convocatoria = new Plantilla(plantilla);
		System.out.println("CANDIDATOS A BASE");
		System.out.println("=================");
		System.out.println();
		ArrayList<Jugador> bases = convocatoria.candidatos(Posicion.PG);
		Iterator<Jugador> j = bases.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A ESCOLTA");
		System.out.println("====================");
		System.out.println();
		ArrayList<Jugador> escoltas = convocatoria.candidatos(Posicion.SG);
		j = escoltas.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A ALERO");
		System.out.println("==================");
		System.out.println();
		ArrayList<Jugador> aleros = convocatoria.candidatos(Posicion.SF);
		j = aleros.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A ALA-PIVOT");
		System.out.println("======================");
		System.out.println();
		ArrayList<Jugador> alas = convocatoria.candidatos(Posicion.PF);
		j = alas.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
		
		System.out.println();
		System.out.println("CANDIDATOS A PIVOT");
		System.out.println("=================");
		System.out.println();
		ArrayList<Jugador> pivots = convocatoria.candidatos(Posicion.C);
		j = pivots.iterator();
		while ( j.hasNext() ) System.out.println(j.next().getNombre());
	}

}
